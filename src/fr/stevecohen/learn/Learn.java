package fr.stevecohen.learn;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.URL;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import fr.stevecohen.autoupdater.AutoUpdater;
import fr.stevecohen.autoupdater.AutoUpdaterException;
import fr.stevecohen.learn.analytics.GoogleAnalytics;
import fr.stevecohen.learn.calcPanels.CarrePanel;
import fr.stevecohen.learn.calcPanels.CubePanel;
import fr.stevecohen.learn.calcPanels.TablePanel;
import fr.stevecohen.learn.otherPanels.ConfigPanel;
import fr.stevecohen.learn.otherPanels.ErrorPanel;
import fr.stevecohen.learn.otherPanels.StatsPanel;
import fr.stevecohen.learn.otherPanels.TrainingPanel;
import fr.stevecohen.util.StatsUtil;

public class Learn implements ComponentListener {

	public static final String			AUTOR = "Steve";
	public static final String			VERSION = "0.7.5";
	public static final String			TITLE = "ApprendTesTables";
	public static final String			VERSIONS_URL = "http://stevecohen.fr/dl/ApprendsTesTables/ApprendsTesTables.json"; //NE JAMAIS CHANGER
//		public static final String			VERSIONS_URL = "http://localhost/autoupdater/ApprendTesTables/ApprendTesTables.json";
	//	private static final float			FONT_SIZE_RATIO = 14.0f/700.0f;
	//	private ScheduledExecutorService 	scheduler = Executors.newScheduledThreadPool(1);
	//	private ScheduledFuture<?>			resizeValidateDelay;

	private JFrame frame;

	private TablePanel					tablePanel;
	private CarrePanel					carrePanel;
	private CubePanel					cubePanel;
	private TrainingPanel				trainingPanel;
	private ErrorPanel					errorPanel;
	private StatsPanel					statsPanel;
	private ConfigPanel					configPanel;

	private GoogleAnalytics 			ga = GoogleAnalytics.getInstance();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					new Learn();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Learn() {
		initializeGA();
		initializeFrame();
	}

	private void initializeGA() {
		ga.trackEvent("System", "Launch application", "Version: " + VERSION, 0);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initializeFrame() {
		setLookAndFeel();
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle(TITLE + " - v" + VERSION + " by " + AUTOR);
		
		URL iconURL = getClass().getResource("/icon.png");
		if (iconURL != null) {
			frame.setIconImage(new ImageIcon(iconURL).getImage());
		}

		tablePanel = new TablePanel();
		carrePanel = new CarrePanel();
		cubePanel = new CubePanel();
		trainingPanel = new TrainingPanel();
		errorPanel = new ErrorPanel();
		statsPanel = new StatsPanel();
		configPanel = new ConfigPanel();

		JTabbedPane tabbedPane = new JTabbedPane();

		tabbedPane.addTab("Tables", tablePanel);
		tabbedPane.addTab("Carrés", carrePanel);
		tabbedPane.addTab("Cubes", cubePanel);
		tabbedPane.addTab("Entrainement", trainingPanel);
		tabbedPane.addTab("Tes erreurs", errorPanel);
		tabbedPane.addTab("Statistiques", statsPanel);
		tabbedPane.addTab("Configuration", configPanel);

		tabbedPane.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				//ga.trackPage(tabbedPane.getSelectedComponent(), tabbedPane.getTitleAt(tabbedPane.getSelectedIndex()));
				ga.trackView(tabbedPane.getTitleAt(tabbedPane.getSelectedIndex()));
			}
		});

		trainingPanel.setTabbedPanel(tabbedPane);

		frame.setContentPane(tabbedPane);
		changeFontSize(14);
		frame.addComponentListener(this);
		try {
			StatsUtil.loadFromDisk();
			for (int i = 0; i < StatsUtil.stats.length(); i++)
				statsPanel.addSession(StatsUtil.stats.getInt(i));
		} catch (Exception e2) {
			e2.printStackTrace();
		}
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				if (StatsUtil.statsLoaded < StatsUtil.stats.length()) {
					int confirmed = JOptionPane.showConfirmDialog(null, 
							"Sauvegarder les résultats des entrainement ?", "Exit",
							JOptionPane.YES_NO_OPTION);
					if (confirmed == JOptionPane.YES_OPTION) {
						try {
							StatsUtil.saveToDisk();
						} catch (Exception e1) {
							e1.printStackTrace();
						}finally {
							ga.close();
						}
					}
				}
				frame.dispose();
			}
		});
		frame.pack();
		frame.invalidate();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		ga.trackPage(tabbedPane.getSelectedComponent(), tabbedPane.getTitleAt(tabbedPane.getSelectedIndex()));

		AutoUpdater autoUpdater = new AutoUpdater(frame, VERSIONS_URL);

		new Thread() {
			public void run() {
				try {
					if (autoUpdater.checkUpdate(VERSION))
						System.out.println("Update needed");
					else
						System.out.println("No update needed");
				} catch (AutoUpdaterException e) {
					e.printStackTrace();
				}
			}
		}.start();

		//		DynaUpdater dynUpdater = new DynaUpdater();
		//		try {
		//			dynUpdater.checkUpdate("http://stevecohen.fr/files/test/ApprendsTesTables");
		//		} catch (Sha1ParseException e1) {
		//			e1.printStackTrace();
		//		} catch (URISyntaxException e1) {
		//			e1.printStackTrace();
		//		} catch (IOException e1) {
		//			e1.printStackTrace();
		//		}
	}

	private void setLookAndFeel() {
		try {
			//			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
			//				if ("Nimbus".equals(info.getName())) {
			//					UIManager.setLookAndFeel(info.getClassName());
			//					break;
			//				}
			//			}
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
		}
	}

	private void changeFontSize(final int size) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				for (Map.Entry<Object, Object> entry : javax.swing.UIManager.getDefaults().entrySet()) {
					Object key = entry.getKey();
					Object value = javax.swing.UIManager.get(key);
					if (value != null && value instanceof javax.swing.plaf.FontUIResource) {
						javax.swing.plaf.FontUIResource fr = (javax.swing.plaf.FontUIResource)value;
						javax.swing.plaf.FontUIResource f = new javax.swing.plaf.FontUIResource(fr.getFamily(), Font.PLAIN, size);
						javax.swing.UIManager.put(key, f);
						if (frame != null)
							SwingUtilities.updateComponentTreeUI(frame);
					}
				}
			}
		});
	}

	@Override
	public void componentHidden(ComponentEvent e) {

	}

	@Override
	public void componentMoved(ComponentEvent e) {

	}

	@Override
	public void componentResized(ComponentEvent e) {
		//		if (resizeValidateDelay != null)
		//			resizeValidateDelay.cancel(true);
		//		resizeValidateDelay = scheduler.schedule(new Runnable() {
		//
		//			@Override
		//			public void run() {
		//				int windowWidth = frame.getWidth();
		//				int fontSize = (int) ((float)windowWidth * FONT_SIZE_RATIO);
		//				changeFontSize(fontSize);
		//			}
		//		}, 500, TimeUnit.MILLISECONDS);
	}

	@Override
	public void componentShown(ComponentEvent e) {

	}
}