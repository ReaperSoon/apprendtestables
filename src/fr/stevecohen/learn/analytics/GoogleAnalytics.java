package fr.stevecohen.learn.analytics;

import com.brsanthu.googleanalytics.AppViewHit;
import com.brsanthu.googleanalytics.EventHit;
import com.brsanthu.googleanalytics.GoogleAnalyticsConfig;
import com.brsanthu.googleanalytics.PageViewHit;

import fr.stevecohen.learn.Learn;
import fr.stevecohen.util.OSChecker;

public class GoogleAnalytics {
	private static final String 									GA_ID ="UA-67854272-7";
	
	private static GoogleAnalytics 									instance;
	private com.brsanthu.googleanalytics.GoogleAnalytics 			ga;

	private GoogleAnalytics() {
		GoogleAnalyticsConfig config = new GoogleAnalyticsConfig();
		config.setUserAgent(OSChecker.getOS());
		ga = new com.brsanthu.googleanalytics.GoogleAnalytics(config, GA_ID, Learn.TITLE, Learn.VERSION);
	}

	public static GoogleAnalytics getInstance() {
		if (instance == null) {
			instance = new GoogleAnalytics();
		}
		return instance;
	}
	
	public void trackPage(Object obj, String title) {
		ga.postAsync(new PageViewHit(obj.getClass().getName().replaceAll("\\.", "/"), title));
	}
	
	public void trackView(String title) {
		ga.postAsync(new AppViewHit(Learn.TITLE, Learn.VERSION, title));
	}
	
	public void trackEvent(String category, String action, String label, Integer value) {
		ga.postAsync(new EventHit(category, action, label, value));
	}
	
	public void close() {
		ga.close();
	}
}
