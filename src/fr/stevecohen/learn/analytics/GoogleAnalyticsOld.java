package fr.stevecohen.learn.analytics;

import com.dmurph.tracking.AnalyticsConfigData;
import com.dmurph.tracking.JGoogleAnalyticsTracker;
import com.dmurph.tracking.JGoogleAnalyticsTracker.DispatchMode;
import com.dmurph.tracking.JGoogleAnalyticsTracker.GoogleAnalyticsVersion;

import fr.stevecohen.learn.Learn;
import fr.stevecohen.util.Util;

public class GoogleAnalyticsOld {
	private static final String GA_ID ="UA-67854272-6";
	private static final GoogleAnalyticsVersion GA_VERSION = GoogleAnalyticsVersion.V_4_7_2;

	private static GoogleAnalyticsOld instance;
	private AnalyticsConfigData config;
	private JGoogleAnalyticsTracker tracker;

	private GoogleAnalyticsOld() {}

	public static GoogleAnalyticsOld getInstance() {
		if (instance == null) {
			instance = new GoogleAnalyticsOld();
		}
		return instance;
	}

	public void init() {
		if (Util.testInet()) {
			JGoogleAnalyticsTracker.setProxy(System.getenv("http_proxy")); 
			config = new AnalyticsConfigData(GA_ID);
			config.populateFromSystem();
			tracker = new JGoogleAnalyticsTracker(config, GA_VERSION);
			tracker.setDispatchMode(DispatchMode.SINGLE_THREAD);
		}else {
			System.err.println("Cannot provide Analytics");
		}
	}

	public void trackEvent(String category, String action, String label) {
		if (Util.testInet()) {
			tracker.trackEvent(category, action, label);
		}
	}

	public void trackPage(Object obj, String title) {
		if (Util.testInet()) {
			//System.out.println("Tracking " + obj.getClass().getName().replaceAll("\\.", "/") + " ; " + title);
			tracker.trackPageView(obj.getClass().getName().replaceAll("\\.", "/"), title, Learn.TITLE);
		}
	}
}
