package fr.stevecohen.learn.calcPanels;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fr.stevecohen.eventBus.EventBus;
import fr.stevecohen.eventBus.EventBus.EventCallback;
import fr.stevecohen.util.Calc;
import fr.stevecohen.util.Util;

public abstract class AbstractCalcPanel extends JPanel implements ActionListener, KeyListener {
	private static final long serialVersionUID = 6822186343296846430L;

	protected Util							util;
	protected EventBus						eventBus;

	protected Calc							calc;

	protected JPanel 						calcPanel;

	protected JLabel 						calcLabel;
	protected JTextField 					resInput;
	protected JButton 						checkBtn;
	protected JLabel 						helpLabel;
	protected JButton 						getRepBtn;

	public AbstractCalcPanel() {
		super(new BorderLayout(10, 10));

		setPreferredSize(new Dimension(700, 300));

		util = Util.getInstance();
		eventBus = EventBus.getEventBus();

		calcPanel = new JPanel();
		calcPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));

		initCalcPanel();
		listenConfigChange();

		calcPanel.add(calcLabel);
		calcPanel.add(resInput);
		calcPanel.add(checkBtn);
		calcPanel.add(getRepBtn);

		add(calcPanel, BorderLayout.NORTH);
		add(helpLabel, BorderLayout.SOUTH);
		
		init();
	}
	
	protected void init() {
		generateCalc();
	}

	public abstract void generateCalc();

	protected void initCalcPanel() {
		calcLabel = new JLabel();
		resInput = new JTextField();
		checkBtn = new JButton("Répondre");
		helpLabel = new JLabel(" ");
		getRepBtn = new JButton("Avoir la réponse");

		resInput.setColumns(5);
		resInput.addKeyListener(this);

		getRepBtn.addActionListener(this);
		getRepBtn.setEnabled(false);

		checkBtn.addActionListener(this);
	}

	protected void listenConfigChange() {
		eventBus.on("ConfigChange", new EventCallback() {

			@Override
			public void call(Object argument) {
				generateCalc();
			}
		});
	}

	protected void checkResponse() {
		if (!resInput.getText().isEmpty()) {
			try {
				int rep = Integer.valueOf(resInput.getText());
				if (rep == calc.getRes()) { 		// TRUE
//					helpLabel.setText("Bravo " + util.getPetitNom() + "!");
					helpLabel.setText("Bravo!");
					generateCalc();
					resInput.setText("");
					getRepBtn.setEnabled(false);
				}else {								//FALSE
//					helpLabel.setText("Raté! Essaie encore " + util.getPetitNom() + ".");
					helpLabel.setText("Raté! Essaie encore.");
					getRepBtn.setEnabled(true);
					eventBus.dispatch("error", calc);
				}
			}catch (NumberFormatException nfe) {
				helpLabel.setText("Invalide value : '" + resInput.getText() + "'");
			}
		}
	}
	
	protected void giveResponse() {
//		helpLabel.setText("La réponse est '" + calc.getRes() + "'. Apprend le " + util.getPetitNom() + "!!!");
		helpLabel.setText("La réponse est '" + calc.getRes() + "'.");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == checkBtn) {
			checkResponse();
		} else if (e.getSource() == getRepBtn) {
			giveResponse();
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_ENTER)
			checkBtn.doClick();
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

}
