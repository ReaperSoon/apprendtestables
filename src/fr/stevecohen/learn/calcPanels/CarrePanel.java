package fr.stevecohen.learn.calcPanels;

import fr.stevecohen.util.Calc;
import fr.stevecohen.util.Config;

public class CarrePanel extends AbstractCalcPanel {

	private static final long serialVersionUID = 7354086330038910159L;
	
	@Override
	public void generateCalc() {
		calc = new Calc(util.getRandBetween(Config.MIN_CARRE, Config.MAX_CARRE), util.getRandBetween(Config.MIN_CARRE, Config.MAX_CARRE), Calc.Type.CARRE);
		calcLabel.setText(calc.num1 + "² = ");
	}

}
