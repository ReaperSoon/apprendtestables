package fr.stevecohen.learn.calcPanels;

import fr.stevecohen.util.Calc;
import fr.stevecohen.util.Config;

public class CubePanel extends AbstractCalcPanel {

	private static final long serialVersionUID = 3322694712183851060L;
	
	@Override
	public void generateCalc() {
		calc = new Calc(util.getRandBetween(Config.MIN_CUBE, Config.MAX_CUBE), util.getRandBetween(Config.MIN_CUBE, Config.MAX_CUBE), Calc.Type.CUBE);
		calcLabel.setText(calc.num1 + "³ = ");
	}
}
