package fr.stevecohen.learn.calcPanels;

import fr.stevecohen.util.Calc;
import fr.stevecohen.util.Config;

public class TablePanel extends AbstractCalcPanel {
	
	private static final long serialVersionUID = -3669737161270060624L;
	
	@Override
	public void generateCalc() {
		calc = new Calc(util.getRandBetween(Config.MIN_TABLE, Config.MAX_TABLE), util.getRandBetween(Config.MIN_TABLE, Config.MAX_TABLE), Calc.Type.TABLE);
		calcLabel.setText(calc.num1 + " x " + calc.num2 + " = ");
	}

}
