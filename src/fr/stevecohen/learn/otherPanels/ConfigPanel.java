package fr.stevecohen.learn.otherPanels;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import fr.stevecohen.eventBus.EventBus;
import fr.stevecohen.learn.analytics.GoogleAnalytics;
import fr.stevecohen.util.Config;

public class ConfigPanel extends JPanel implements ActionListener {

	private static final long serialVersionUID = -4462606683164242435L;
	
	private EventBus							eventBus;
	
	private JTextField 							tableMin;
	private JTextField 							tableMax;
	
	private JTextField 							carreMin;
	private JTextField 							carreMax;
	
	private JTextField 							cubeMin;
	private JTextField 							cubeMax;
	
	private JTextField							questNbr;
	private JTextField							questTmp;
	
	private JButton 							save;
	
	final JLabel 								saved;
	
	private GoogleAnalytics						ga;

	public ConfigPanel() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		eventBus = EventBus.getEventBus();
		ga = GoogleAnalytics.getInstance();
		
		JPanel table = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 10));
		JPanel carre = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 10));
		JPanel cube = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 10));
		JPanel calcs = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 10));
		JPanel button = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 10));
		JPanel label = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 10));
		
		JPanel train = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 10));
		
		table.setBorder(new TitledBorder(null, "Tables", TitledBorder.CENTER, TitledBorder.TOP, null, Color.BLACK));
		carre.setBorder(new TitledBorder(null, "Carrés", TitledBorder.CENTER, TitledBorder.TOP, null, Color.BLACK));
		cube.setBorder(new TitledBorder(null, "Cubes", TitledBorder.CENTER, TitledBorder.TOP, null, Color.BLACK));
		train.setBorder(new TitledBorder(null, "Entrainement", TitledBorder.CENTER, TitledBorder.TOP, null, Color.BLACK));

		tableMin = new JTextField();
		tableMin.setText(String.valueOf(Config.MIN_TABLE));
		tableMin.setColumns(2);
		
		tableMax = new JTextField();
		tableMax.setText(String.valueOf(Config.MAX_TABLE));
		tableMax.setColumns(2);
		
		carreMin = new JTextField();
		carreMin.setText(String.valueOf(Config.MIN_CARRE));
		carreMin.setColumns(2);
		
		carreMax = new JTextField();
		carreMax.setText(String.valueOf(Config.MAX_CARRE));
		carreMax.setColumns(2);
		
		cubeMin = new JTextField();
		cubeMin.setText(String.valueOf(Config.MIN_CUBE));
		cubeMin.setColumns(2);
		
		cubeMax = new JTextField();
		cubeMax.setText(String.valueOf(Config.MAX_CUBE));
		cubeMax.setColumns(2);
		
		questNbr = new JTextField();
		questNbr.setText(String.valueOf(Config.TRAINING_QUESTIONS));
		questNbr.setColumns(2);
		
		questTmp = new JTextField();
		questTmp.setText(String.valueOf(Config.TRAINING_TIMER));
		questTmp.setColumns(2);

		save = new JButton("Sauvegarder");
		save.addActionListener(this);
		saved = new JLabel(" ");

		table.add(new JLabel("Min : "));
		table.add(tableMin);
		table.add(new JLabel("Max : "));
		table.add(tableMax);

		carre.add(new JLabel("Min : "));
		carre.add(carreMin);
		carre.add(new JLabel("Max : "));
		carre.add(carreMax);

		cube.add(new JLabel("Min : "));
		cube.add(cubeMin);
		cube.add(new JLabel("Max : "));
		cube.add(cubeMax);
		
		train.add(new JLabel("Questions : "));
		train.add(questNbr);
		train.add(new JLabel("Temps : "));
		train.add(questTmp);

		calcs.add(table);
		calcs.add(carre);
		calcs.add(cube);
		
		button.add(save);
		label.add(saved);
		
		add(calcs);
		add(train);
		add(label);
		add(button);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == save) {
			int minTable = Integer.parseInt(tableMin.getText());
			int maxTable = Integer.parseInt(tableMax.getText());
			int minCarre = Integer.parseInt(carreMin.getText());
			int maxCarre = Integer.parseInt(carreMax.getText());
			int minCube = Integer.parseInt(cubeMin.getText());
			int maxCube = Integer.parseInt(cubeMax.getText());
			int trainingQuest = Integer.parseInt(questNbr.getText());
			int trainingTimer = Integer.parseInt(questTmp.getText());
			
			if (Config.MIN_TABLE != minTable) {
				Config.MIN_TABLE = minTable;
				ga.trackEvent("User", "Configuration change", "minTable", minTable);
			}
			if (Config.MAX_TABLE != maxTable) {
				Config.MAX_TABLE = maxTable;
				ga.trackEvent("User", "Configuration change", "maxTable", maxTable);
			}
			if (Config.MIN_CARRE != minCarre) {
				Config.MIN_CARRE = minCarre;
				ga.trackEvent("User", "Configuration change", "minCarre", minCarre);
			}
			if (Config.MAX_CARRE != maxCarre) {
				Config.MAX_CARRE = maxCarre;
				ga.trackEvent("User", "Configuration change", "maxCarre", maxCarre);
			}
			if (Config.MIN_CUBE != minCube) {
				Config.MIN_CUBE = minCube;
				ga.trackEvent("User", "Configuration change", "minCube", minCube);
			}
			if (Config.MAX_CUBE != maxCube) {
				Config.MAX_CUBE = maxCube;
				ga.trackEvent("User", "Configuration change", "maxCube", maxCube);
			}
			if (Config.TRAINING_QUESTIONS != trainingQuest) {
				Config.TRAINING_QUESTIONS = trainingQuest;
				ga.trackEvent("User", "Configuration change", "trainingQuest", trainingQuest);
			}
			if (Config.TRAINING_TIMER != trainingTimer) {
				Config.TRAINING_TIMER = trainingTimer;
				ga.trackEvent("User", "Configuration change", "trainingTimer", trainingTimer);
			}
			
			saved.setText("Configuration sauvegardée");
			
			eventBus.dispatch("ConfigChange", null);
		}
	}
}
