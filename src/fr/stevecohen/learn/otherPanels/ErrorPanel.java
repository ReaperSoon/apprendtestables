package fr.stevecohen.learn.otherPanels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

import fr.stevecohen.eventBus.EventBus;
import fr.stevecohen.util.Calc;
import fr.stevecohen.util.WrapLayout;

public class ErrorPanel extends JPanel implements ActionListener {

	private static final long serialVersionUID = -5001834395762195349L;

	private EventBus					eventBus;

	private JScrollPane					scrollPane;
	private JPanel						mainPanel;
	private JPanel						tableErrorsPanel;
	private JPanel						carreErrorsPanel;
	private JPanel						cubeErrorsPanel;

	private JButton 					resetBtn;

	public ErrorPanel() {
		setLayout(new BorderLayout(10, 10));
		mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		
		tableErrorsPanel = new JPanel(new WrapLayout(FlowLayout.LEFT, 10, 10));
		carreErrorsPanel = new JPanel(new WrapLayout(FlowLayout.LEFT, 10, 10));
		cubeErrorsPanel = new JPanel(new WrapLayout(FlowLayout.LEFT, 10, 10));
		
		tableErrorsPanel.setBorder(new TitledBorder(null, "Tables", TitledBorder.CENTER, TitledBorder.TOP, null, Color.BLACK));
		carreErrorsPanel.setBorder(new TitledBorder(null, "Carrés", TitledBorder.CENTER, TitledBorder.TOP, null, Color.BLACK));
		cubeErrorsPanel.setBorder(new TitledBorder(null, "Cubes", TitledBorder.CENTER, TitledBorder.TOP, null, Color.BLACK));
		
		mainPanel.add(tableErrorsPanel);
		mainPanel.add(carreErrorsPanel);
		mainPanel.add(cubeErrorsPanel);
		
		scrollPane = new JScrollPane(mainPanel);
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		eventBus = EventBus.getEventBus();

		resetBtn = new JButton("Reset");
		resetBtn.addActionListener(this);

		add(new JLabel("Tes erreurs (passe le curseur dessus pour voir les réponses) :"), BorderLayout.NORTH);
		add(scrollPane, BorderLayout.CENTER);
		add(resetBtn, BorderLayout.SOUTH);

		listenForErrors();
	}

	private void listenForErrors() {
		eventBus.on("error", new EventBus.EventCallback() {

			@Override
			public void call(Object argument) {
				Calc calc = (Calc) argument;
				JPanel myErrorPanel = calc.type == Calc.Type.TABLE ? tableErrorsPanel : (calc.type == Calc.Type.CARRE ? carreErrorsPanel : cubeErrorsPanel);
				if (notAlreadyShown(myErrorPanel, calc.toString())) {
					JLabel errorLbl = new JLabel();
					errorLbl.setText(calc.toString());
					errorLbl.setToolTipText(String.valueOf(calc.getRes()));
					errorLbl.setPreferredSize(new Dimension(getParent().getWidth() / 10, getParent().getHeight() / 10));
					errorLbl.setHorizontalAlignment(SwingConstants.CENTER);
					errorLbl.setVerticalAlignment(SwingConstants.CENTER);

					myErrorPanel.add(errorLbl);
				}
			}
		});
	}
	
	private boolean notAlreadyShown(JPanel myErrorPanel, String calc) {
		for (int i = 0; i < myErrorPanel.getComponentCount(); i++) {
			Component comp = myErrorPanel.getComponent(i);
			if (comp instanceof JLabel && ((JLabel)comp).getText().equals(calc))
				return false;
		}
		return true;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == resetBtn) {
			tableErrorsPanel.removeAll();
			tableErrorsPanel.repaint();
			carreErrorsPanel.removeAll();
			carreErrorsPanel.repaint();
			cubeErrorsPanel.removeAll();
			cubeErrorsPanel.repaint();
		}
	}


}
