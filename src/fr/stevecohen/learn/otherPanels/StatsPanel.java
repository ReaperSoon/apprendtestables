package fr.stevecohen.learn.otherPanels;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import fr.stevecohen.eventBus.EventBus;
import fr.stevecohen.eventBus.EventBus.EventCallback;
import fr.stevecohen.util.StatsUtil;

public class StatsPanel extends JPanel {

	private static final long serialVersionUID = -7588226259272531198L;
	
	private EventBus					eventBus;
	
	private XYSeries 					sessionSerie;
	private XYDataset 					dataset;
	private JFreeChart 					chart;
	
	private int							xIndex = 1;

	
	public StatsPanel() {
		super(new BorderLayout(10, 10));
		eventBus = EventBus.getEventBus();
		listenForStats();
		sessionSerie = new XYSeries("Sessions");
		dataset = new XYSeriesCollection();
		((XYSeriesCollection) dataset).addSeries(sessionSerie);
		chart = createChart(dataset);

		ChartPanel chartPanel = new ChartPanel(chart);
		chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
		add(chartPanel, BorderLayout.CENTER);
	}
	
	private JFreeChart createChart(XYDataset dataset2) {
		final JFreeChart chart = ChartFactory.createXYLineChart(
				"Evolution des entrainements",      // chart title
				"Entrainements",            // x axis label
				"Résultats (sur 20)",          // y axis label
				dataset,              // data
				PlotOrientation.VERTICAL,
				false,                // include legend
				false,                // tooltips
				false                 // urls
				);
		chart.setBackgroundPaint(Color.white);

		final XYPlot plot = chart.getXYPlot();
		plot.setBackgroundPaint(Color.lightGray);
		plot.setDomainGridlinePaint(Color.white);
		plot.setRangeGridlinePaint(Color.white);
		
		final XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
		renderer.setSeriesLinesVisible(0, true);
		renderer.setSeriesShapesVisible(0, true);
		plot.setRenderer(renderer);

		final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
		rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		
		final NumberAxis domain = (NumberAxis) plot.getDomainAxis();
        domain.setTickUnit(new NumberTickUnit(5));
        
        NumberAxis range = (NumberAxis) plot.getRangeAxis();
        range.setRange(0, 20);
        range.setTickUnit(new NumberTickUnit(2));

		return chart;
	}
	
	public void addSession(Integer y) {
		sessionSerie.add(xIndex++, y);
	}
	
	private void listenForStats() {
		eventBus.on("Stats", new EventCallback() {
			
			@Override
			public void call(Object argument) {
				Integer nbr = (Integer)argument;
				addSession(nbr);
				StatsUtil.stats.put(nbr);
			}
		});
	}
}
