package fr.stevecohen.learn.otherPanels;

import java.awt.Dimension;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;

import fr.stevecohen.eventBus.EventBus.EventCallback;
import fr.stevecohen.learn.analytics.GoogleAnalytics;
import fr.stevecohen.learn.calcPanels.AbstractCalcPanel;
import fr.stevecohen.util.Calc;
import fr.stevecohen.util.Config;

public class TrainingPanel extends AbstractCalcPanel {

	private ScheduledExecutorService 	scheduler = Executors.newScheduledThreadPool(1);
	private ScheduledFuture<?>			responseSchedule;
	private int							currentTime;
	private int							currentQuestionIndex;

	private int							goodAnswer;
	private JTabbedPane 				tabbedPane;
	
	private GoogleAnalytics				ga;

	private static final long serialVersionUID = -6596330121559782211L;

	public TrainingPanel() {
		super();
		ga = GoogleAnalytics.getInstance();
		Dimension repBtnDim = getRepBtn.getPreferredSize();
		repBtnDim.width = 100;
		getRepBtn.setMinimumSize(repBtnDim);
		getRepBtn.setText("Commencer (" + (Config.TRAINING_TIMER) + "sec)");
		getRepBtn.setEnabled(true);
		resInput.setEnabled(false);
		checkBtn.setEnabled(false);
		helpLabel.setText("Clique sur 'Commencer' quand tu es prêt! Tu as " + Config.TRAINING_TIMER + " secondes pour répondre aux questions");
		calcLabel.setText("...");
		calcLabel.setHorizontalAlignment(SwingConstants.CENTER);
		listenConfigChange();
	}
	
	@Override
	protected void listenConfigChange() {
		eventBus.on("ConfigChange", new EventCallback() {

			@Override
			public void call(Object argument) {
				getRepBtn.setText("Commencer (" + (Config.TRAINING_TIMER) + "sec)");
			}
		});
	};

	@Override
	protected void init() {}

	@Override
	public void generateCalc() {
		int type = util.getRandBetween(1, 3);
		switch (type) {
		case 1: //Table
			calc = new Calc(util.getRandBetween(Config.MIN_TABLE, Config.MAX_TABLE), util.getRandBetween(Config.MIN_TABLE, Config.MAX_TABLE), Calc.Type.TABLE);
			calcLabel.setText(calc.num1 + " x " + calc.num2 + " = ");
			break;
		case 2: //Carré
			calc = new Calc(util.getRandBetween(Config.MIN_CARRE, Config.MAX_CARRE), util.getRandBetween(Config.MIN_CARRE, Config.MAX_CARRE), Calc.Type.CARRE);
			calcLabel.setText(calc.num1 + "² = ");
			break;
		case 3: //Cube
			calc = new Calc(util.getRandBetween(Config.MIN_CUBE, Config.MAX_CUBE), util.getRandBetween(Config.MIN_CUBE, Config.MAX_CUBE), Calc.Type.CUBE);
			calcLabel.setText(calc.num1 + "³ = ");
			break;
		}
	}

	@Override
	public void checkResponse() {
		resInput.requestFocus();
		
		if (!resInput.getText().isEmpty() && Integer.valueOf(resInput.getText()) == calc.getRes()) { //GOOD
			goodAnswer++;
		}else { //WRONG
			eventBus.dispatch("error", calc);
		}
		
		currentQuestionIndex++;
		
		if (currentQuestionIndex <= Config.TRAINING_QUESTIONS) { //NOT FINISHED
			responseSchedule.cancel(true);
			generateCalc();
			helpLabel.setText(goodAnswer + "/" + (currentQuestionIndex-1) + " bonne réponses (" + (100*goodAnswer/(currentQuestionIndex-1)) + "%). Question " + (currentQuestionIndex-1) + " sur " + Config.TRAINING_QUESTIONS);
			startCountdown(Config.TRAINING_TIMER);
		}else {													//FINISHED
			calcLabel.setText("");
			responseSchedule.cancel(true);
			getRepBtn.setEnabled(true);
			tabbedPane.setEnabled(true);
			checkBtn.setEnabled(false);
			resInput.setEnabled(false);
			int percent_success = (100*goodAnswer/Config.TRAINING_QUESTIONS);
			helpLabel.setText("Résultats: " + goodAnswer + " bonnes réponses sur les " + Config.TRAINING_QUESTIONS + " questions (" + percent_success + "%)");
			eventBus.dispatch("Stats", (int)(20.0f*(float)goodAnswer/(float)Config.TRAINING_QUESTIONS));
			ga.trackEvent("Training", "finish", "result (%)", percent_success);
			getRepBtn.setText("Recommencer");
		}
		resInput.setText("");
	}

	private void startCountdown(int time) {
		currentTime = time;
		getRepBtn.setText(String.valueOf(currentTime) + "sec");
		responseSchedule = scheduler.scheduleAtFixedRate(new Runnable() {

			@Override
			public void run() {
				currentTime--;
				getRepBtn.setText(String.valueOf(currentTime) + "sec");
				if (currentTime == 0) { //AFTER TRAINING_TIMER seconds
					checkResponse();
				}
			}
		}, 1, 1, TimeUnit.SECONDS);
	}

	@Override
	protected void giveResponse() { //START CLICKED
		tabbedPane.setEnabled(false);
		getRepBtn.setEnabled(false);
		checkBtn.setEnabled(true);
		resInput.setEnabled(true);
		currentQuestionIndex = 1;
		helpLabel.setText("Résultats: " + goodAnswer + " bonnes réponses sur les " + Config.TRAINING_QUESTIONS + " questions (" + (100*goodAnswer/Config.TRAINING_QUESTIONS) + "%)");
		goodAnswer = 0;
		resInput.requestFocus();
		generateCalc();
		startCountdown(Config.TRAINING_TIMER);
	}

	public void setTabbedPanel(JTabbedPane tabbedPane) {
		this.tabbedPane = tabbedPane;
	}
}
