package fr.stevecohen.util;

public class Calc {

	public Type type;
	public int num1;
	public int num2;

	public Calc(int num1, int num2, Type type) {
		this.num1 = num1;
		this.num2 = num2;
		this.type = type;
	}

	public Calc(int num1, Type type) {
		this.num1 = num1;
		this.type = type;
	}

	public int getRes() {
		int myres = -1;
		if (type == Type.TABLE) {
			myres = num1 * num2;
		}else if (type == Type.CARRE) {
			myres = num1 * num1;
		}else if (type == Type.CUBE) {
			myres = num1 * num1 * num1;
		}
		return myres;
	}

	public enum Type {
		TABLE,
		CARRE,
		CUBE
	}
	
	@Override
	public String toString() {
		String value = "Bad type";
		if (type == Type.TABLE) {
			value = num1 + " x " + num2;
		}else if (type == Type.CARRE) {
			value = num1 + "²";
		}else if (type == Type.CUBE) {
			value = num1 + "³";
		}
		return value;
	}

}
