package fr.stevecohen.util;

public class Config {
	
	public static int							TRAINING_QUESTIONS = 20;
	public static int 							TRAINING_TIMER = 5;
	
	public static int							MIN_TABLE = 1;
	public static int							MAX_TABLE = 20;
	
	public static int							MIN_CARRE = 1;
	public static int							MAX_CARRE = 20;
	
	public static int							MIN_CUBE = 1;
	public static int							MAX_CUBE = 20;
	
}
