package fr.stevecohen.util;

import java.io.File;
import java.io.PrintWriter;

import org.json.JSONArray;
import org.json.JSONTokener;

public class StatsUtil {

	private static final String						JSON_FILE_PATH = "stats";

	public static JSONArray							stats;

	public static int								statsLoaded;

	public static void loadFromDisk() throws Exception {
		File dir = new File(Util.getApprendTesTablesDir());
		if (dir.mkdirs() || dir.exists()) {
			File file = new File(dir, JSON_FILE_PATH);
			if (file.exists()) {
				JSONTokener tokener = new JSONTokener(file.toURI().toURL().openStream());
				try {
					stats = new JSONArray(tokener);
				}catch (Exception e) {
					stats = new JSONArray();
				}
			}else {
				file.createNewFile();
				stats = new JSONArray();
				saveToDisk();
			}
			statsLoaded = stats.length();
			System.out.println(statsLoaded + " results loaded!");
		}else {
			throw new RuntimeException("Cannot create working directory: " + dir);
		}
	}

	public static void saveToDisk() throws Exception {
		File dir = new File(Util.getApprendTesTablesDir());
		File file = new File(dir, JSON_FILE_PATH);
		PrintWriter writer = new PrintWriter(file, "UTF-8");
		writer.println(stats.toString(2));
		writer.close();
		System.out.println((stats.length() - statsLoaded) + " new results saved!");
	}
}
