package fr.stevecohen.util;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.AccessController;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.util.Random;

import fr.stevecohen.learn.Learn;

public class Util {

	private static Util					instance;
	private static Random				rand;
	private final static String			TEST_INET = "www.google.fr";


	private Util() {
		rand = new Random();
	}
	
	public static boolean testInet() {
	    Socket sock = new Socket();
	    InetSocketAddress addr = new InetSocketAddress(TEST_INET, 80);
	    try {
	        sock.connect(addr, 3000);
	        return true;
	    } catch (IOException e) {
	    	e.printStackTrace();
	        return false;
	    } finally {
	        try {sock.close();}
	        catch (IOException e) {}
	    }
	}

	public static Util getInstance() {
		if (instance == null)
			instance = new Util();
		return instance;
	}
	
	public int getRandBetween(int min, int max) {
		if (min == max) {
            return max;
        }

        return rand.nextInt(max - min + 1) + min;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static String getApprendTesTablesDir() throws PrivilegedActionException {
		return (String)AccessController.doPrivileged(new PrivilegedExceptionAction() {
			public Object run() throws Exception {
				return Util.getWorkingDirectory(Learn.TITLE) + File.separator;
			}
		});
	}

	private static File getWorkingDirectory(String applicationName) {
		String userHome = System.getProperty("user.home", ".");
		File workingDirectory;
		if (OSChecker.isUnix() || OSChecker.isSolaris()) {
			workingDirectory = new File(userHome, '.' + applicationName + '/');
		} else if (OSChecker.isWindows()) {
			String applicationData = System.getenv("APPDATA");
			if (applicationData != null)
				workingDirectory = new File(applicationData, "." + applicationName + '/');
			else
				workingDirectory = new File(userHome, '.' + applicationName + '/');
		} else if (OSChecker.isMac()) {
			workingDirectory = new File(userHome, "Library/Application Support/" + applicationName);
		} else {
			workingDirectory = new File(userHome, applicationName + '/');
		}
		return workingDirectory;
	}

}
